package policyengine

import (
	"bytes"
	"context"
	"fmt"
	"github.com/open-policy-agent/opa/rego"
	"github.com/open-policy-agent/opa/storage"
	"github.com/open-policy-agent/opa/storage/inmem"
)

var policyModel *rego.Rego

// Policy describes REGO policy definition with version
type Policy struct {
	Version string `json:"version"`
	Data    string `json:"data"`
}

// Initialise the Rego policy
func init() {
	policyModel = rego.New(
		rego.Query("data.rbac.policy.allow"),
		rego.Module("rbac_example", initialPolicy()),
		rego.Store(initialPolicyData()),
	)
}

// EvaluatePolicy
func CheckAuthZStatus(input map[string]interface{}) (bool, error) {
	allow := false
	ctx := context.Background()

	pr, err := policyModel.PartialResult(ctx)
	if err != nil {
		return allow, err
	}

	fmt.Printf("Got this input : %v\n", input)
	// Prepare and run normal evaluation from the result of partial
	// evaluation.
	r := pr.Rego(
		rego.Input(input),
	)

	rs, err := r.Eval(ctx)

	if err != nil || len(rs) != 1 || len(rs[0].Expressions) != 1 {
		// Handle erorr.
		fmt.Println("Error when evaluating the request")
	} else {
		outcome := rs[0].Expressions[0].Value
		allow = outcome.(bool)
		fmt.Println("Evaluated the result to ", allow)
	}
	return allow, err
}

func initialPolicy() string {
	return `package rbac.policy

	import data.user_roles
	import data.role_permissions
	import input

	default allow = false

	allow {
		# lookup the list of roles for the user
		roles := user_roles[input.user]
	
		# for each role in that list
		r := roles[_]
	
		# lookup the permissions list for role r
		permissions := role_permissions[r]
	
		# for each permission
		p := permissions[_]
	
		# check if the permission granted to r matches the user's request
		p == {"action": input.action, "resource": input.resource}
	}`
}

func initialPolicyData() storage.Store {
	return inmem.NewFromReader(bytes.NewBufferString(`{
    "resources" : [
        "/coderepo/project1", 
        "/hr/employeedb"
    ],
    "actions" : [
        "read", 
        "write"
    ],
    "roles" : [
        "administrator", 
        "developer",
        "hr"
    ],
    "role_permissions" : {
        "administrator" : [{"action": "read",  "resource": "/coderepo/project1"}],
        "developer" :      [{"action": "read",  "resource": "/coderepo/project1"},
                            {"action": "write", "resource": "/coderepo/project1"}],
        "hr" :          [{"action": "read",  "resource": "/hr/employeedb"}]
    },
	"user_roles" : {
        "alice" : ["developer"],
        "bob" : ["administrator", "hr"],
        "charlie" : ["administrator"]
    }
}`))
}
