module rbac-off-blockchain/server

go 1.15

require github.com/open-policy-agent/opa v0.25.2
require github.com/gorilla/mux v1.8.0