package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"rbac-off-blockchain/server/policyengine"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", homePage)
	router.HandleFunc("/pdp/authstatus", checkAuthZStatus).Methods("POST")
	log.Fatal(http.ListenAndServe(":9091", router))
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

func checkAuthZStatus(w http.ResponseWriter, r *http.Request) {
	// get the body of our POST request
	reqBody, _ := ioutil.ReadAll(r.Body)
	var input map[string]interface{}
	json.Unmarshal(reqBody, &input)

	allowed, err := policyengine.CheckAuthZStatus(input)
	if err != nil {
		fmt.Fprintf(w, "%+v", err)
	}
	fmt.Fprintf(w, "%+v", allowed)
}
