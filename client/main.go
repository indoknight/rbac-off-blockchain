package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	reqBodyBytes := []byte(`{
				"user" : "alice",
				"action" : "read",
				"resource" : "/hr/employeedb"
			 }`)
	resp, err := http.Post("http://localhost:9091/pdp/authstatus", "application/json", bytes.NewBuffer(reqBodyBytes))
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	respBodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println("Got response string ", string(respBodyBytes))
}
