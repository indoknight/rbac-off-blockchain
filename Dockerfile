FROM golang:1.15

RUN mkdir /server

ADD ./server /server

WORKDIR /server

RUN go build rbac-off-blockchain/server

CMD go run rbac-off-blockchain/server
